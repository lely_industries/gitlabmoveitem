#!/usr/bin/env bash
#
# This script updates the package files used by the docker file `docker\Dockerfile`.
# Effectively pinning all dependencies used. This allows for reproducible docker images.
#
set -Eeuo pipefail
shopt -s extglob
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")"
cat /etc/os-release || cat /usr/lib/os-release

# apt - apt_packages.txt
while read -r PACKAGE_FILE <&3; do
    : >|"docker/${PACKAGE_FILE}"
    while read -r PACKAGE <&4; do
        PACKAGE_VERSION=$(
            apt-cache policy "${PACKAGE}" | grep --regexp="^\s*Candidate: "
        ) || true
        echo "${PACKAGE}${PACKAGE_VERSION/*([[:space:]])Candidate: /=}" | tee --append "docker/${PACKAGE_FILE}"
    done 4<"${PACKAGE_FILE}"
done 3<<EOF
apt_packages.txt
EOF

# python pip
while read -r REQUIREMENTS; do
    python3 -m venv --clear /tmp/.venv-autoupdate
    . /tmp/.venv-autoupdate/bin/activate
    pip install \
        --upgrade \
        pip \
        wheel
    pip install \
        --requirement "${REQUIREMENTS}" \
        --upgrade
    pip freeze | grep -v "pkg.resources" >|docker/"${REQUIREMENTS}"
    deactivate ""
done <<EOF
requirements.txt
requirements-dev.txt
EOF

# Cleanup
rm -rf /tmp/.venv-autoupdate

# ./docker/docker.sh -e -- ./autoupdate.sh
