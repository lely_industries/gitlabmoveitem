# Gitlab move item

-   Free software: Apache Software License 2.0

The [Gitlab] server does not allow you to move or rename a project that has a [Docker] registry
containing any entries. Nor does it allow you to move or rename groups that contain such projects.\
(Using Project/Group -> Settings -> General -> Advanced -> Change path or Transfer)

The [Gitlab API] does not have an option for moving groups.

Using both the [Docker CLI] and the [Gitlab API] you can download and then upload renamed images
from and to the [Docker] registry. You can remove the [Docker] registry from a project to allow it
to be moved or renamed, temporary un-archiving it if so required.

You can move a group using the [Gitlab API] by creating the target desired group structure by
downloading an archive of it and then uploading it to the desired location. And then move all
projects from the old group to the new group. And remove the old group afterwards. Then correct all
shares.

We need this functionality to restructure our explosively grown [Gitlab] server to preserve
maintainability.

## Known issues

Group move is done by copy - delete.

-   <https://gitlab.com/gitlab-org/gitlab/-/issues/23831>
-   Tests verify projects, Docker images and shares. Anything else is not verified.
-   Project id and creation dates will change.

<https://gitlab.com/gitlab-org/gitlab/-/issues/215715>

## Documentation

To use the project:

-   Create a [python-gitlab configuration] where you can safely add full admin API read-only rights
    to a [Gitlab] server.
    -   The module uses the default or the one specified with a parameter.
    -   The unit test uses `test_write` and requires a full admin read/write API key.
-   Have [Python] 3 installed (Hint: `python3-venv`)
-   Create a [Python] virtual environment:
    ```
    python3 -m venv venv
    ```
    or
    ```
    py -m venv venv
    ```
-   Activate it:
    ```
    . ./venv/bin/activate
    ```
    or
    ```
    venv\Scripts\activate.bat
    ```
-   Install required packages:
    ```
    pip install -U pip
    pip install -r requirements.txt
    pip install -e .
    ```
-   Check the documentation:
    ```
    gitlab_move_item --help
    ```
-   Move something in read-only log mode:
    ```
    gitlab_move_item log foo/bar bar
    ```
-   Use it in Python:
    ```
    import gitlab_move_item.cli
    gitlab_move_item(...)
    ```

## Development

To run all the tests run:

```
tox
```

#

[gitlab api]: https://docs.gitlab.com/ee/api/README.html
[gitlab]: https://about.gitlab.com/
[docker]: https://www.docker.com/
[docker cli]: https://docs.docker.com/engine/reference/commandline/cli/
[public lely gitlab repository]: https://gitlab.com/lely_industries
[.gitlab-ci-yml]: https://docs.gitlab.com/ee/ci/yaml/
[include]: https://docs.gitlab.com/ee/ci/yaml/#include
[python-gitlab]: https://github.com/python-gitlab/python-gitlab/blob/master/README.rst
[python-gitlab configuration]: https://python-gitlab.readthedocs.io/en/stable/cli.html#configuration
[python]: https://www.python.org/
