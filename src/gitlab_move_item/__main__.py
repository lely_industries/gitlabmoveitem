"""
Entrypoint module, in case you use `python -mgitlab_move_item`.


Why does this file exist, and why __main__? For more info, read:

- https://www.python.org/dev/peps/pep-0338/
- https://docs.python.org/2/using/cmdline.html#cmdoption-m
- https://docs.python.org/3/using/cmdline.html#cmdoption-m
"""
import sys

from gitlab_move_item.cli import cli


def main(args=None):
    """."""
    if __name__ == "__main__":
        sys.exit(cli(args=args))


main()
