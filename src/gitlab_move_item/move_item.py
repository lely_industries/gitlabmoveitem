"""."""
import copy
import posixpath
import subprocess
from http import HTTPStatus
from urllib.parse import urlparse

from gitlab import Gitlab
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects import Group

from . import docker_repository
from . import python_gitlab_extentions

ACTION = python_gitlab_extentions.ACTION


class GitlabMoveItemException(Exception):
    """Our own exception to distinguish it from all others."""


class GitlabMoveItem(Gitlab):
    """."""

    def __new__(cls, url, *args, **kwargs):
        """."""
        if url is None:
            kwargs.pop("action", None)
            return super().from_config(*args, **kwargs)
        return super().__new__(cls)

    def __init__(self, url, *args, action=None, **kwargs):
        """."""
        if url is not None:
            kwargs.setdefault("retry_transient_errors", True)
            super().__init__(url, *args, **kwargs)
        if action is None:
            self._action = ACTION.LOG
        elif isinstance(action, str):
            self._action = ACTION[action.upper()]
        else:
            self._action = ACTION(action)
        self.authenticated_docker_urls = set()
        self._docker_binary = None
        self.auth()

    @classmethod
    def from_config(cls, *args, **kwargs):
        """."""
        return cls(None, *args, **kwargs)

    @property
    def action(self):
        """."""
        return self._action

    @property
    def docker_binary(self):
        """."""
        if not self._docker_binary:
            self._docker_binary = docker_repository.docker_or_podman()
        return self._docker_binary

    @property
    def auth_token(self):
        """."""
        return self.private_token or self.oauth_token or self.job_token or self.http_password

    def docker(self, command, *args, **kwargs):
        """."""
        action = self.action
        if self.docker_binary == "podman" and command in {"login", "pull", "push"}:
            args = ("--tls-verify=false", *args)
        if action == ACTION.MOVE or (action == ACTION.CACHE and command.casefold() == "pull"):
            try:  # https://gitlab.com/gitlab-org/gitlab/-/issues/215715
                subprocess.run([self.docker_binary, command, *args], check=True, **kwargs)
            except subprocess.CalledProcessError:
                if command.casefold() != "push":
                    raise
                subprocess.run([self.docker_binary, command, *args], check=True, **kwargs)
        else:
            print(" ".join((self.docker_binary, command, *args)))

    def docker_authenticate(self, url):
        """."""
        url = urlparse(url).netloc
        if url not in self.authenticated_docker_urls:
            self.docker(
                "login",
                "--password-stdin",
                "--username",
                self.user.username,
                url,
                text=True,
                input=self.auth_token,
            )
            self.authenticated_docker_urls |= {url}

    def verify_paths(self, source_path, target_path):
        """."""
        source_path = source_path and posixpath.normpath(source_path).lstrip(posixpath.sep)
        source = self.get_item(source_path)
        if not source:
            raise GitlabMoveItemException(f"Source `{source_path}` does not exist.")

        target_path = target_path and posixpath.normpath(target_path).lstrip(posixpath.sep)
        target = self.get_item(target_path)
        if target:
            raise GitlabMoveItemException(
                f"Target `{target_path}` already exist as "
                f"{target.__class__} {target.id} `{target.full_path}`."
            )

        target_group_path, target_name = posixpath.split(target_path)
        target_group_path.lstrip(posixpath.sep)
        target_group = self.get_item(target_group_path)
        if not target_group:
            raise GitlabMoveItemException(f"Target group `{target_group_path}` does not exist.")
        if not isinstance(target_group, Group):
            raise GitlabMoveItemException(
                f"Target group `{target_group_path}` exist as "
                f"{target_group.__class__} {target_group.id} `{target_group.full_path}`.\n"
                f"Target group must be a {Group}."
            )

        if target_group.full_path.startswith(source.full_path):
            raise GitlabMoveItemException(
                f"You cannot move {source.__class__} {source.id} `{source.full_path}\n"
                f"to itself or its child "
                f"({target_group.__class__} {target_group.id} `{target_group.full_path})."
            )

        return source, target_group, target_name

    def get_item(self, id_or_path, *paths):
        """."""
        name = posixpath.join(id_or_path, *paths)
        try:
            return self.projects.get(name)
        except GitlabGetError as ex:
            if not ex.response_code == HTTPStatus.NOT_FOUND:
                raise
        try:
            return self.groups.get(name, with_projects=False)
        except GitlabGetError as ex:
            if not ex.response_code == HTTPStatus.NOT_FOUND:
                raise
        try:
            return self.namespaces.get(name)
        except GitlabGetError as ex:
            if not ex.response_code == HTTPStatus.NOT_FOUND:
                raise
        return None

    def move_rename(self, source_path, target_path):
        """."""
        source, target_group, target_name = self.verify_paths(source_path, target_path)
        print(
            f"\n{self.action.name.lower()} `{source.full_path}` to "
            f"`{posixpath.join(target_group.full_path, target_name)}` on '{self.url}'"
        )
        source_docker_images = docker_repository.DockerRepository()

        if self.action == ACTION.CACHE:
            print("\nCaching images ...\n")
            source.remove_images(source_docker_images)
            return

        try:
            print("\nSaving and removing images ...\n")
            source.remove_images(source_docker_images)

            print("\nRenaming images ...\n")
            target_docker_images = copy.deepcopy(source_docker_images)
            target_docker_images.rename(source, target_group, target_name)

            print(f"\nMoving/Renaming {source.__class__} {source.id} `{source.full_path}` ...\n")
            source.move_rename(target_group, target_name)

        except:
            print("\nRestoring images ...\n")
            source_docker_images.upload(self)
            raise
        print("\nUploading renamed images ...\n")
        exception = target_docker_images.upload(self)
        if exception is not None:
            print("\nNot removing temporary images to allow recovery ...\n")
            raise exception

        print("\nRemoving temporary images ...\n")
        source_docker_images.remove(self)
        target_docker_images.remove(self)


def gitlab_move_item(source, target, url, *args, **kwargs):
    """Parameters
    From python-gitlab: Gitlab()
        https://python-gitlab.readthedocs.io/en/stable/api/gitlab.html
    From python-gitlab: Gitlab.from_config()
        https://python-gitlab.readthedocs.io/en/stable/api/gitlab.html#gitlab.Gitlab.from_config
    The action to perform from move_item.ACTION
        action=None
    """
    with GitlabMoveItem(url, *args, **kwargs) as repo:
        return repo.move_rename(source, target)
