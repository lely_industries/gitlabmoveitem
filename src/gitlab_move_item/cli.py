#!/usr/bin/env python
"""."""
import argparse
import os
import sys

import gitlab_move_item

URL_PARAMETER = "url"
URL_PARAMETERS = [
    "http_username",
    "http_password",
    "private_token",
    "oauth_token",
    "job_token",
]
CONFIG_FILE_PARAMETERS = ["gitlab_id", "config_files"]


def add_environment_argument(group, parameter, default):
    """."""
    environment_name = "_".join((gitlab_move_item.__package__, parameter)).upper()
    environment_value = os.environ.get(environment_name)
    group.add_argument(
        "".join(("--", parameter)),
        default=environment_value or default,
        help=f"or {os.path.basename(environment_name)}",
    )


def cli(args=None):
    """."""
    parser = argparse.ArgumentParser(
        epilog="A parameter without a value uses the value of the environment variable."
    )
    parser.add_argument(
        "--version",
        action="version",
        version=f"{gitlab_move_item.__package__} {gitlab_move_item.__version__}",
    )
    parser.add_argument(
        "action",
        choices=(action.name.lower() for action in gitlab_move_item.ACTION),
        metavar=f"{{{','.join((action.name.lower() for action in gitlab_move_item.ACTION))}}}",
        help="Action to perform",
    )
    parser.add_argument("source")
    parser.add_argument("target")

    group = parser.add_argument_group("By parameter", "See the `python-gitlab` documentation.")
    add_environment_argument(group, URL_PARAMETER, None)
    group = group.add_mutually_exclusive_group()
    for parameter in URL_PARAMETERS:
        add_environment_argument(group, parameter, argparse.SUPPRESS)

    group = parser.add_argument_group(
        "By `python-gitlab` configuration file", "Used if there is no URL parameter."
    )
    for parameter in CONFIG_FILE_PARAMETERS:
        add_environment_argument(group, parameter, argparse.SUPPRESS)

    return gitlab_move_item.gitlab_move_item(**vars(parser.parse_args(args=args)))


def main(args=None):
    """."""
    if __name__ == "__main__":
        sys.exit(cli(args=args))


main()
