"""."""
import contextlib
import enum
import functools
import posixpath
import uuid

from gitlab.mixins import ListMixin
from gitlab.v4.objects import Group
from gitlab.v4.objects import Project
from gitlab.v4.objects import ProjectRegistryRepository
from gitlab.v4.objects import ProjectRegistryTag

ACTION = enum.Enum("ACTION", "LOG CACHE MOVE")

# Change the default behavior of returning lists of items (Projects/Groups/...)
# to return non-paginated lists.

setattr(ListMixin, ListMixin.list.__name__, functools.partialmethod(ListMixin.list, iterator=True))


def add_method(cls, name=None):
    """We add methodes to Project/Group/... by `setattr` instead of sub-classing
    because our actual intention is to have a proper parent class `Item`
    from which Project/Group/... inherit.
    See functions `full_path`, `parent_id` and `move_item_share` to quickly understand why."""

    def decorator(func):
        setattr(cls, name or func.__name__, func)
        return func

    return decorator


@add_method(Project, "full_path")
@property
def full_path(self):
    """Group already has this property holding the same information."""
    return self.path_with_namespace


@add_method(Project, "parent_id")
@property
def parent_id(self):
    """Group already has this property holding the same information."""
    return self.namespace["id"]


@add_method(Project)
@add_method(Group)
def move(self, target_group):
    """."""
    if self.manager.gitlab.action == ACTION.MOVE:
        self.transfer(target_group.id)
    else:
        print(f"Moving:       `{self.full_path}` to `{target_group.full_path}{posixpath.sep}`")


@add_method(Project)
@add_method(Group)
def rename(self, target_name):
    """."""
    if self.manager.gitlab.action == ACTION.MOVE:
        self.path = target_name
        self.save()
    else:
        print(f"Renaming:     `{self.full_path}` to `{target_name}`")
        # Pretend, but we do not save.
        self.path = target_name


@add_method(Project)
@add_method(Group)
def move_rename(self, target_group, target_name):
    """."""
    if self.parent_id != target_group.id:
        repo = self.manager.gitlab
        if repo.get_item(target_group.full_path, self.path):
            source_group_path = posixpath.dirname(self.full_path).lstrip(posixpath.sep)
            name = target_name
            while repo.get_item(source_group_path, name) or repo.get_item(
                target_group.full_path, name
            ):
                name = "".join((target_name, uuid.uuid4().hex))
            self.rename(name)
        self.move(target_group)
    if self.path != target_name:
        self.rename(target_name)


@add_method(Project)
@contextlib.contextmanager
def writable_archive(self):
    """."""
    action = self.manager.gitlab.action
    if action == ACTION.MOVE and self.archived:
        try:
            self.unarchive()
            yield self
        finally:
            self.archive()
    elif action == ACTION.LOG and self.archived:
        try:
            print(f"Un-archiving: `{self.full_path}`")
            yield self
        finally:
            print(f"Re-archiving: `{self.full_path}`")
    else:
        try:
            yield self
        finally:
            pass


@add_method(Project, "remove_images")
def remove_images_from_project(self, image_store):
    """."""
    if not self.container_registry_enabled:
        return
    repositories = self.repositories.list()
    if not repositories:
        return
    repo = self.manager.gitlab
    with self.writable_archive():
        images = image_store[self.id]
        for repository in repositories:
            for tag in repository.tags.list():
                repo.docker_authenticate("".join(("//", tag.location)))
                repo.docker("pull", tag.location)
                images.append(tag.location)
                tag.remove()
            repository.remove()


@add_method(Group, "remove_images")
def remove_images_from_group(self, image_store):
    """."""
    repo = self.manager.gitlab
    for project in self.projects.list(include_subgroups=True, simple=True, with_shared=False):
        project = repo.projects.get(project.id)
        project.remove_images(image_store)


@add_method(ProjectRegistryTag, "remove")
def remove_image_from_project_registry(self):
    """."""
    action = self.manager.gitlab.action
    if action == ACTION.MOVE:
        self.delete()
    elif action == ACTION.LOG:
        print(f"Removing:   {self.location}")
    else:
        pass


@add_method(ProjectRegistryRepository, "remove")
def remove_project_registry(self):
    """."""
    action = self.manager.gitlab.action
    if action == ACTION.MOVE:
        self.delete()
    elif action == ACTION.LOG:
        print(f"Removing:   {self.location}")
    else:
        pass
