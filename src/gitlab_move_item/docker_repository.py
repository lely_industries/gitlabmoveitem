"""."""
import posixpath
import subprocess
from warnings import warn

from gitlab.exceptions import GitlabError


class DockerRepository(dict):
    """."""

    def __missing__(self, key):
        """."""
        self[key] = []
        return self[key]

    @staticmethod
    def upload_project_images(repo, project, images):
        """Upload all images from a project."""
        exception = None
        with project.writable_archive():
            for image in images:
                try:
                    repo.docker_authenticate("".join(("//", image)))
                    repo.docker("push", image)
                except subprocess.SubprocessError as ex:
                    exception = exception or ex
                    print(f"{repo.docker_binary} push {image}")
        return exception

    def upload(self, repo):
        """Upload all collected images, printing any command that failed."""
        exception = None
        for project_id, images in self.items():
            try:
                project = repo.projects.get(project_id)
                ex = self.upload_project_images(repo, project, images)
                exception = exception or ex
            except GitlabError as ex:
                exception = exception or ex
                warn(str(ex))
                for image in images:
                    print(f"{repo.docker_binary} push {image}")
        return exception

    def remove(self, repo):
        """."""
        for images in self.values():
            for image in images:
                repo.docker("image", "rm", "--force", image)

    def rename(self, source, target_group, target_name):
        """."""

        def rename_inplace_generator(repo, source_path, target_path):
            def rename_inplace(image):
                url, sep, new_name = image.partition(posixpath.sep)
                new_name, colon, tag = new_name.partition(":")
                new_name = new_name.replace(source_path, target_path, 1)
                new_name = "".join((url, sep, new_name, colon, tag))
                repo.docker("tag", image, new_name)
                return new_name

            return rename_inplace

        repo = source.manager.gitlab
        source_path = source.full_path
        target_path = posixpath.join(target_group.full_path, target_name)
        rename_inplace = rename_inplace_generator(repo, source_path, target_path)
        for images in self.values():
            images[:] = [rename_inplace(image) for image in images]


def docker_or_podman():
    """."""
    kwargs = {"stdout": subprocess.PIPE, "stderr": subprocess.STDOUT, "text": True}
    result = None
    for docker in ("podman", "docker"):
        try:
            result = subprocess.run([docker, "--version"], **kwargs, check=True)
        except (OSError, subprocess.CalledProcessError) as ex:
            result = ex
        else:
            print(f"{docker}: {result.stdout}")
            return docker
    raise result
