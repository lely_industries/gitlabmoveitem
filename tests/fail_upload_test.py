"""."""
import posixpath
import subprocess
from contextlib import nullcontext

import pytest
from gitlab.exceptions import GitlabError
from pytest import fixture

from gitlab_move_item.docker_repository import docker_or_podman
from gitlab_move_item.move_item import GitlabMoveItem


@fixture(name="prepare_project")
def fixture_prepare_project(create_group, create_project, add_docker_image_to_project):
    """."""
    source_project = create_project("source_project")
    add_docker_image_to_project(source_project)
    source_project.archive()

    target_group = create_group("target_group")
    target_project_path_with_namespace = posixpath.join(target_group.full_path, source_project.path)

    yield source_project.path_with_namespace, target_project_path_with_namespace


@pytest.mark.parametrize(
    "test_parameters",
    [
        ("pull", GitlabError, nullcontext()),
        ("push", GitlabError, pytest.warns(UserWarning)),
        ("push", subprocess.SubprocessError, nullcontext()),
    ],
)
def test_fail_upload(move_item, prepare_project, monkeypatch, test_parameters):
    """."""
    (test_command, exception, expected_warning) = test_parameters
    original_docker = GitlabMoveItem.docker

    def mock_docker(self, command, *args, **kwargs):
        """."""
        if command.casefold() == test_command:
            raise exception
        original_docker(self, command, *args, **kwargs)

    with monkeypatch.context() as localpatch:
        localpatch.setattr(GitlabMoveItem, "docker", mock_docker)

        with pytest.raises(exception), expected_warning:
            move_item(*prepare_project)


def test_docker_not_found(monkeypatch):
    """."""

    def mock_subprocess(*args, **kwargs):
        """."""
        raise subprocess.CalledProcessError(returncode=-1, cmd=None)

    with monkeypatch.context() as localpatch:
        localpatch.setattr(subprocess, "run", mock_subprocess)

        with pytest.raises(subprocess.CalledProcessError):
            docker_or_podman()
