"""."""
import posixpath

import pytest

from gitlab_move_item.move_item import GitlabMoveItemException


def test_source_not_exist(move_item, base_group):
    """."""
    source_project_path_with_namespace = posixpath.join(base_group.full_path, "source_project")
    target_project_path_with_namespace = posixpath.join(base_group.full_path, "target_project")

    with pytest.raises(GitlabMoveItemException, match=r"Source `.*` does not exist."):
        move_item(
            source_project_path_with_namespace,
            target_project_path_with_namespace,
        )


def test_target_group_not_exist(move_item, base_group, create_project):
    """."""
    source_project = create_project("source_project")
    target_project_path_with_namespace = posixpath.join(
        base_group.full_path, "target_group", "target_project"
    )

    with pytest.raises(GitlabMoveItemException, match=r"Target group `.*` does not exist."):
        move_item(
            source_project.path_with_namespace,
            target_project_path_with_namespace,
        )


def test_target_already_exist(move_item, create_project):
    """."""
    source_project = create_project("source_project")
    target_project = create_project("target_project")

    with pytest.raises(GitlabMoveItemException, match=r"Target `.*` already exist as .* .* `.*`."):
        move_item(
            source_project.path_with_namespace,
            target_project.path_with_namespace,
        )


def test_target_is_not_group(move_item, create_project):
    """."""
    source_project = create_project("source_project")
    base_project = create_project("base_project")
    target_project_path_with_namespace = posixpath.join(
        base_project.path_with_namespace, "target_project"
    )

    with pytest.raises(
        GitlabMoveItemException,
        match=r"Target group `.*` exist as .* .* `.*`.\nTarget group must be a .*.",
    ):
        move_item(
            source_project.path_with_namespace,
            target_project_path_with_namespace,
        )


def test_target_recursion(move_item, create_group):
    """."""
    source_group = create_group("source_group")
    child_group = create_group("target_group", parent_id=source_group.id)
    target_group_full_path = posixpath.join(child_group.full_path, "target_project")

    with pytest.raises(
        GitlabMoveItemException,
        match=r"You cannot move .* .* `.*\nto itself or its child (.* .* `.*).",
    ):
        move_item(source_group.full_path, target_group_full_path)
