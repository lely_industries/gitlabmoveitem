"""."""
import os
import posixpath
import subprocess
from urllib.parse import urlparse

import pytest
from gitlab import Gitlab
from gitlab import const
from gitlab.exceptions import GitlabDeleteError
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects import Project
from pytest import fixture

import gitlab_move_item
from gitlab_move_item import cli

DOCKER_TEST_IMAGE = "docker.io/docker/getting-started"
TEST_GROUP = {"name": "tests", "path": "tests"}


def create_decorator(repository, parent, base_group):
    """Simplifies the interface of python-gitlab CreateMixin for easier calling,
    but it removes the possibility of adding extra options (**kwargs) like `sudo`."""

    def decorator(func):
        def create(config=None, **kwargs):
            if config is None:
                config = {}
            elif isinstance(config, int):
                config = {"name": config}
            elif isinstance(config, str):
                path, name = posixpath.split(posixpath.normpath(config))
                path.lstrip(posixpath.sep)
                config = {"name": name}
                if path:
                    config[parent] = repository.namespaces.get(path).id
            else:
                pass
            config.update(kwargs)
            if "path" not in config and "name" in config:
                config["path"] = config["name"].casefold()
            if parent not in config:
                config[parent] = base_group.id
            elif config[parent] is None:
                config.remove(parent)
            else:
                pass
            return func(config)

        return create

    return decorator


def add_config_parameters(configuration, parameters):
    """."""
    for parameter in parameters:
        value = os.environ.get("_".join((gitlab_move_item.__package__, parameter)).upper())
        if value is not None:
            configuration[parameter] = value


@fixture(scope="session", name="repository")
def fixture_repository():
    """."""
    configuration = {}
    add_config_parameters(
        configuration,
        [
            cli.URL_PARAMETER,
        ],
    )

    if cli.URL_PARAMETER in configuration:
        add_config_parameters(configuration, cli.URL_PARAMETERS)
        with Gitlab(**configuration) as gitlab_repository:
            gitlab_repository.auth()
            yield gitlab_repository
    else:
        add_config_parameters(configuration, cli.CONFIG_FILE_PARAMETERS)
        with Gitlab.from_config(**configuration) as gitlab_repository:
            gitlab_repository.auth()
            yield gitlab_repository


@fixture(scope="session", name="session_group")
def fixture_session_group(repository):
    """."""
    group = repository.groups.create(TEST_GROUP)
    yield group
    group.delete()


@fixture(name="base_group")
def fixture_base_group(request, repository, session_group):
    """."""
    name = "".join(
        character if character.isascii() and character.isalnum() else "_"
        for character in request.node.nodeid.rpartition("::")[2]
    ).strip("_")
    group = repository.groups.create(
        {
            "name": name,
            "parent_id": session_group.id,
            "path": name.casefold(),
        }
    )
    yield group
    group.delete()


@fixture(name="autoremove")
def fixture_autoremove(repository, base_group):
    """."""
    auto = [base_group.id]
    manual = []

    def create(item):
        parent_id = item.namespace["id"] if isinstance(item, Project) else item.parent_id
        if parent_id not in auto:
            manual.append(item)
        auto.append(item.id)
        return item

    yield create

    while manual:
        item = manual.pop()
        try:
            item.delete()
        except GitlabDeleteError:
            if isinstance(item, Project):
                try:
                    item = repository.projects.get(item.id)
                except GitlabGetError:
                    pass
                else:
                    item.delete()


@fixture(name="create_project")
def fixture_create_project(repository, autoremove, base_group):
    """."""

    @create_decorator(repository, "namespace_id", base_group)
    def create(config, **kwargs):
        return autoremove(repository.projects.create(config, **kwargs))

    yield create


@fixture(name="create_group")
def fixture_create_group(repository, autoremove, base_group):
    """."""

    @create_decorator(repository, "parent_id", base_group)
    def create(config, **kwargs):
        return autoremove(repository.groups.create(config, **kwargs))

    yield create


@fixture(scope="session", name="docker_binary")
def fixture_docker_binary():
    """."""
    yield gitlab_move_item.docker_repository.docker_or_podman()


@fixture(scope="session", name="docker_args")
def fixture_docker_args(docker_binary):
    """."""
    yield ["--tls-verify=false"] if docker_binary == "podman" else []


@fixture(scope="session", name="docker_authenticate")
def fixture_docker_authenticate(repository, docker_binary, docker_args):
    """."""

    def docker_authenticate_generator(username, token):
        """."""
        urls = set()

        def docker_authenticate(url):
            nonlocal urls
            url = urlparse(url).netloc
            if url not in urls:
                subprocess.run(
                    [
                        docker_binary,
                        "login",
                        "--password-stdin",
                        "--username",
                        username,
                        *docker_args,
                        url,
                    ],
                    check=True,
                    text=True,
                    input=token,
                )
                urls |= {url}

        return docker_authenticate

    yield docker_authenticate_generator(
        repository.user.username,
        repository.private_token
        or repository.oauth_token
        or repository.job_token
        or repository.http_password,
    )


@fixture(scope="session", name="docker_test_image")
def fixture_docker_test_image(docker_binary, docker_args):
    """."""
    subprocess.run([docker_binary, "pull", *docker_args, DOCKER_TEST_IMAGE], check=True)
    yield DOCKER_TEST_IMAGE
    subprocess.run([docker_binary, "rmi", "--force", DOCKER_TEST_IMAGE], check=True)


@fixture(name="add_docker_image_to_project")
def fixture_add_docker_image_to_project(
    docker_test_image, docker_authenticate, docker_binary, docker_args
):
    """."""

    def add_image(project):
        tag = posixpath.join(
            project.container_registry_image_prefix, posixpath.split(docker_test_image)[1]
        )
        subprocess.run([docker_binary, "tag", docker_test_image, tag], check=True)
        docker_authenticate("".join(("//", tag)))
        try:  # https://gitlab.com/gitlab-org/gitlab/-/issues/215715
            subprocess.run([docker_binary, "push", *docker_args, tag], check=True)
        except subprocess.CalledProcessError:
            subprocess.run([docker_binary, "push", *docker_args, tag], check=True)
        return project

    yield add_image


@fixture(name="create_share")
def fixture_create_share(base_group):
    """."""

    def create_share(item, group=None, access=None):
        if group is None:
            group = base_group
        if access is None:
            access = const.DEVELOPER_ACCESS
        return item.share(group.id, access)

    yield create_share


@fixture(name="move_item")
def fixture_move_item(monkeypatch):
    """."""
    static_url = None
    static_args = []
    static_kwargs = {}

    def gitlab_move_item_mock(source, target, url, *args, **kwargs):
        """."""
        del source, target
        nonlocal static_url, static_args, static_kwargs
        static_url = url
        static_args = args[:]
        static_kwargs = kwargs.copy()
        static_kwargs.pop("action", None)

    with monkeypatch.context() as localpatch:
        localpatch.setattr(cli, "__name__", "__main__")
        localpatch.setattr(gitlab_move_item, "gitlab_move_item", gitlab_move_item_mock)
        with pytest.raises(SystemExit):
            cli.main([gitlab_move_item.ACTION.LOG.name.casefold(), "", ""])

    def move(source, target, action=None):
        """."""
        gitlab_move_item.gitlab_move_item(
            source, target, static_url, action=action, *static_args, **static_kwargs
        )

    yield move
