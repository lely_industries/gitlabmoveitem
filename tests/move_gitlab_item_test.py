"""."""
import posixpath
from http import HTTPStatus

import pytest
from gitlab import const
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects import Group
from gitlab.v4.objects import Namespace
from gitlab.v4.objects import Project
from pytest import fixture
from pytest_lazyfixture import lazy_fixture

import gitlab_move_item


def dir_name(path):
    """."""
    if path is not None:
        path = posixpath.dirname(path).lstrip(posixpath.sep)
    return path


def get_item_path(item):
    """."""
    if isinstance(item, Project):
        return item.path_with_namespace
    if isinstance(item, (Group, Namespace)):
        return item.full_path
    return None


def get_item(source, target_path):
    """."""
    repo = source.manager.gitlab
    if isinstance(source, Project):
        return repo.projects.get(target_path)
    if isinstance(source, Group):
        return repo.groups.get(target_path)
    if isinstance(source, Namespace):
        return repo.namespaces.get(target_path)
    return None


def filter_attributes(remove, rename):
    """."""

    def decorator(func):
        def _filter(item, old, new):
            attributes = getattr(item, "attributes", item)
            attributes = {
                attribute: value.replace(old, new, 1)
                if old != new and attribute in rename
                else value
                for attribute, value in attributes.items()
                if attribute not in remove
            }
            return func(attributes, old, new)

        return _filter

    return decorator


@filter_attributes(
    {},
    {
        "group_full_path",
    },
)
def filter_share(item, *_):
    """."""
    return item


@filter_attributes(
    {
        "id",
        "name",
        "parent_id",
    },
    {
        "full_path",
        "web_url",
    },
)
def filter_namespace(item, old, new):
    """."""
    if "namespace" in item:
        item["namespace"] = filter_namespace(item["namespace"], old, new)
    item["shared_with_groups"] = [
        filter_share(share, old, new) for share in item.get("shared_with_groups", [])
    ]
    item["shared_with_groups"].sort(key=lambda share: share["group_name"])
    if old != new:
        item["path"] = posixpath.basename(item["full_path"])
    return item


@filter_attributes(
    {
        "name_with_namespace",
        "permissions",
    },
    {
        "container_registry_image_prefix",
        "http_url_to_repo",
        "path_with_namespace",
        "ssh_url_to_repo",
        "web_url",
    },
)
def filter_project(item, old, new):
    """."""
    item["shared_with_groups"] = [
        filter_share(share, old, new) for share in item["shared_with_groups"]
    ]
    item["shared_with_groups"].sort(key=lambda share: share["group_name"])
    if old != new:
        item["path"] = posixpath.basename(item["path_with_namespace"])
    if new == item["path_with_namespace"]:
        old = dir_name(old)
        new = dir_name(new)
    item["namespace"] = filter_namespace(item["namespace"], old, new)
    return item


@filter_attributes(
    {
        "full_name",
        "parent_id",
        "runners_token",
    },
    {
        "full_path",
        "web_url",
    },
)
def filter_group(item, old, new):
    """."""
    item["projects"] = [filter_project(project, old, new) for project in item.get("projects", [])]
    item["shared_projects"] = [
        filter_project(project, old, new) for project in item.get("shared_projects", [])
    ]
    item["shared_with_groups"] = [
        filter_share(share, old, new) for share in item["shared_with_groups"]
    ]
    item["shared_with_groups"].sort(key=lambda share: share["group_name"])
    if old != new:
        item["path"] = posixpath.basename(item["full_path"])
    return item


def filter_item(item, old=None, new=None):
    """."""
    if isinstance(item, Project):
        return filter_project(item, old, new)
    if isinstance(item, Group):
        return filter_group(item, old, new)
    if isinstance(item, Namespace):
        return filter_namespace(item, old, new)
    return None


@fixture(name="rename_project")
def fixture_rename_project(base_group, create_project):
    """."""
    source_project = create_project("source_project")
    target_path = posixpath.join(base_group.full_path, "target_project")
    yield source_project, target_path, None


@fixture(name="rename_group")
def fixture_rename_group(base_group, create_group):
    """."""
    source_group = create_group("source_group")
    target_path = posixpath.join(base_group.full_path, "target_group")
    yield source_group, target_path, None


@fixture(name="move_project")
def fixture_move_project(create_project, create_group):
    """."""
    source_project = create_project("source_project")
    target_group = create_group("target_group")
    target_path = posixpath.join(target_group.full_path, source_project.path)
    yield source_project, target_path, None


@fixture(name="move_project_name_clash")
def fixture_move_project_name_clash(create_project, create_group):
    """."""
    source_project = create_project("source_project")
    clash_project = create_project("target_project")
    target_group = create_group("target_group")
    create_project(clash_project.name, path=source_project.path, namespace_id=target_group.id)
    target_path = posixpath.join(target_group.full_path, clash_project.path)
    yield source_project, target_path, None


@fixture(name="move_group")
def fixture_move_group(create_group):
    """."""
    source_group = create_group("source_group")
    target_group = create_group("target_group")
    target_path = posixpath.join(target_group.full_path, source_group.path)
    yield source_group, target_path, None


@fixture(name="move_tree")
def fixture_move_tree(create_project, add_docker_image_to_project, create_group, create_share):
    """."""
    external_shares = create_group("external_shares")
    project_to_share = create_project("project_share", namespace_id=external_shares.id)

    source_group = create_group("source_group")
    create_share(source_group, access=const.GUEST_ACCESS)

    create_share(project_to_share, source_group)
    create_share(external_shares, source_group)
    create_share(source_group, external_shares)

    create_project("project", namespace_id=source_group.id, container_registry_enabled=False)
    project = create_project(
        "archived", namespace_id=source_group.id, container_registry_enabled=False
    )
    project.archive()

    project = create_project("docker", namespace_id=source_group.id)
    add_docker_image_to_project(project)
    project = create_project("docker_archived", namespace_id=source_group.id)
    add_docker_image_to_project(project)
    project.archive()

    child_group = create_group("child_group", parent_id=source_group.id)
    create_share(project, child_group)
    create_share(external_shares, child_group)
    create_share(project_to_share, child_group)
    create_share(project, external_shares)
    create_share(child_group, external_shares)
    create_share(child_group, source_group)
    create_share(source_group, child_group)

    create_project("child", namespace_id=child_group.id, container_registry_enabled=False)
    project = create_project(
        "child_archived", namespace_id=child_group.id, container_registry_enabled=False
    )
    project.archive()

    project = create_project("child_docker", namespace_id=child_group.id)
    add_docker_image_to_project(project)

    project = create_project("child_docker_archived", namespace_id=child_group.id)
    add_docker_image_to_project(project)
    project.archive()

    target_group = create_group("target_group")
    create_share(target_group, external_shares)
    create_share(external_shares, target_group)

    target_path = posixpath.join(target_group.full_path, source_group.path)
    source_group = source_group.manager.gitlab.groups.get(source_group.id)
    external_shares = external_shares.manager.gitlab.groups.get(external_shares.id)
    yield source_group, target_path, external_shares


@fixture(
    name="prepare_items",
    params=[
        lazy_fixture("rename_project"),
        lazy_fixture("rename_group"),
        lazy_fixture("move_project"),
        lazy_fixture("move_project_name_clash"),
        lazy_fixture("move_group"),
        lazy_fixture("move_tree"),
    ],
)
def fixture_prepare_items(request):
    """."""
    yield request.param


def test_move_item(prepare_items, move_item):
    """."""
    log_test_move_item(*prepare_items, move_item)
    cache_test_move_item(*prepare_items, move_item)
    move_test_move_item(*prepare_items, move_item)


def log_test_move_item(source, target_path, _, move_item):
    """."""
    move_item(get_item_path(source), target_path, None)

    with pytest.raises(GitlabGetError) as ex:
        get_item(source, target_path)
    assert ex.value.response_code == HTTPStatus.NOT_FOUND


def cache_test_move_item(source, target_path, _, move_item):
    """."""
    move_item(get_item_path(source), target_path, gitlab_move_item.ACTION.CACHE)

    with pytest.raises(GitlabGetError) as ex:
        get_item(source, target_path)
    assert ex.value.response_code == HTTPStatus.NOT_FOUND


def move_test_move_item(source, target_path, external_shares, move_item):
    """."""
    source_path = get_item_path(source)
    move_item(source_path, target_path, "move")
    target = get_item(source, target_path)

    source = filter_item(source, source_path, target_path)
    target = filter_item(target)
    assert source == target

    if external_shares is not None:
        verify = external_shares.manager.gitlab.groups.get(external_shares.id)
        external_shares = filter_item(external_shares, source_path, target_path)
        verify = filter_item(verify)
        assert external_shares == verify
