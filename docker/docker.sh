#!/usr/bin/env bash
set -Eeuo pipefail

# Parse parameters
# See `man getopt'. https://gist.github.com/bobpaul/ecd74cdf7681516703f20726431eaceb
SHORT="l::w:f::d::a::s::i:r::p:v::n::u::e::ch"
LONG="executable::,local-dir::,work-dir:,docker-file::,docker-dir::,build-arg::,secret::,image:,"
LONG+="image-registry::,image-path:,image-version::,name::,user::,userns::,entrypoint::,no-cache"
LONG+=",help"
USAGE="Usage: $(basename -- "$0") [OPTIONS] [--] [COMMANDS]
Options:
      --executable      Use docker instead of podman EXECUTABLE=${EXECUTABLE-}
  -l, --local-dir       LOCAL_DIR=${LOCAL_DIR-}
  -w, --work-dir        WORK_DIR=${WORK_DIR-}
  -f, --docker-file     DOCKER_FILE=${DOCKER_FILE-}
  -d, --docker-dir      DOCKER_DIR=${DOCKER_DIR-}
  -a, --build-arg       DOCKER_BUILD_ARGS[*]=${DOCKER_BUILD_ARGS[*]}
            DOCKER_BUILD_ARGS+=(\"--build-arg=\$0\")
  -s, --secret          DOCKER_SECRETS[*]=${DOCKER_SECRETS[*]}
            DOCKER_SECRETS+=(\"--secret=\$0\")
  -i, --image           IMAGE=${IMAGE-}
  -r, --image-registry  IMAGE_REGISTRY=${IMAGE_REGISTRY-}
  -p, --image-path      IMAGE_PATH=${IMAGE_PATH-}
  -v, --image-version   IMAGE_VERSION=${IMAGE_VERSION-}
  -n, --name            PROJECT=${PROJECT-}
  -u, --user            USER_IN_CONTAINER=${USER_IN_CONTAINER-}
      --userns          USERNS=${USERNS-}
  -e, --entrypoint      ENTRYPOINT=${ENTRYPOINT-}
  -c, --no-cache        NO_WRITE_CACHE=${NO_WRITE_CACHE-}
  -h, --help            Prints this help text and exits.
Default command:        COMMAND=${COMMAND-}

Builds and runs the projects container adjusted by the options and commands provided.
"

! (getopt --test >/dev/null || [[ $? -ne 4 ]]) || ! echo getopt is outdated
PARSED=$(getopt --options "${SHORT}" --longoptions "${LONG}" --name "$0" -- "$@")
eval set -- "${PARSED}"
while [[ $# -gt 0 ]]; do
    case "$1" in
    --executable)
        shift
        EXECUTABLE="$1"
        : "${EXECUTABLE:=docker}"
        ;;
    -l | --local-dir)
        shift
        LOCAL_DIR="$1"
        ;;
    -w | --work-dir)
        shift
        WORK_DIR="$1"
        ;;
    -f | --docker-file)
        shift
        DOCKER_FILE="$1"
        ;;
    -d | --docker-dir)
        shift
        DOCKER_DIR="$1"
        ;;
    -a | --build-arg)
        shift
        if [[ -z $1 ]]; then
            unset DOCKER_BUILD_ARGS
        else
            DOCKER_BUILD_ARGS+=("--build-arg=$1")
        fi
        ;;
    -s | --secret)
        shift
        if [[ -z $1 ]]; then
            unset DOCKER_SECRETS
        else
            DOCKER_SECRETS+=("--secret=$1")
        fi
        ;;
    -i | --image)
        shift
        IMAGE="$1"
        ;;
    -r | --image-registry)
        shift
        IMAGE_REGISTRY="$1"
        ;;
    -p | --image-path)
        shift
        IMAGE_PATH="$1"
        ;;
    -v | --image-version)
        shift
        IMAGE_VERSION="$1"
        ;;
    -n | --name)
        shift
        PROJECT="$1"
        ;;
    -u | --user)
        shift
        USER_IN_CONTAINER="$1"
        ;;
    --userns)
        shift
        USERNS="$1"
        ;;
    -e | --entrypoint)
        shift
        ENTRYPOINT="$1"
        ;;
    -h | --help)
        SHOW_HELP=1
        ;;
    -c | --no-cache)
        NO_WRITE_CACHE=1
        ;;
    --)
        shift
        break
        ;;
    *)
        echo "Error:  Cannot parse '$*'"
        exit 1
        ;;
    esac
    shift
done

# Defaults
WPWD="$(pwd -W 2>/dev/null || pwd || true)"
: "${EXECUTABLE:=podman}"
ARTIFACTORY_DOMAIN="artifactory.lelyonline.com"
: "${DOCKER_FILE="${BASH_SOURCE[0]/"$(basename -- "${BASH_SOURCE[0]}")"/"Dockerfile"}"}"
: "${DOCKER_DIR="$(git -C "$(dirname -- "${DOCKER_FILE}")" rev-parse --show-toplevel 2>/dev/null || true)"}"
[[ -n ${DOCKER_FILE-} ]] && DOCKER_FILE="$(realpath "--relative-to=${WPWD}" -- "${DOCKER_FILE}" || true)"
[[ -n ${DOCKER_DIR-} ]] && DOCKER_DIR="$(realpath "--relative-to=${WPWD}" -- "${DOCKER_DIR}" || true)"
if [[ -z ${IMAGE-} ]]; then
    : "${IMAGE_REGISTRY="${ARTIFACTORY_DOMAIN}"}"
    IMAGE_REGISTRY="${IMAGE_REGISTRY##*://}"
    if [[ -z ${IMAGE_PATH-} ]]; then
        IMAGE_PATH="$(git -C "${DOCKER_DIR}" ls-remote --get-url 2>/dev/null || true)"
        if [[ -n ${IMAGE_PATH} ]]; then
            IMAGE_PATH="${IMAGE_PATH#*:}"
            IMAGE_PATH="${IMAGE_PATH%.git}"
            if [[ ${IMAGE_REGISTRY} == "${ARTIFACTORY_DOMAIN}" ]]; then
                IMAGE_PATH="${IMAGE_PATH%%/*}-docker-dev-local/${IMAGE_PATH}"
            fi
            IMAGE_PATH="${IMAGE_REGISTRY}/${IMAGE_PATH}"
        fi
    fi
    IMAGE_PATH="${IMAGE_PATH%/}"
    IMAGE_PATH="${IMAGE_PATH#/}"
    : "${IMAGE_VERSION="$(git -C "${DOCKER_DIR}" rev-list --max-count=1 HEAD -- 2>/dev/null || true)"}"
    IMAGE="${IMAGE_PATH:+"${IMAGE_PATH}${IMAGE_VERSION:+":${IMAGE_VERSION}"}"}"
else
    IMAGE="${IMAGE##*://}"
    if [[ ${IMAGE%:*} == "${IMAGE}" || ${IMAGE#"${IMAGE%:*}":} == */* ]]; then
        IMAGE_PATH="${IMAGE}"
        IMAGE_VERSION=""
    else
        IMAGE_PATH="${IMAGE%:*}"
        IMAGE_VERSION="${IMAGE#"${IMAGE%:*}":}"
    fi
    IMAGE_PATH="${IMAGE_PATH%/}"
    : "${IMAGE_REGISTRY="${IMAGE_PATH%%/*}"}"
    IMAGE_PATH="${IMAGE_PATH#/}"
fi
: "${PROJECT="$(basename -- "${IMAGE_PATH}")"}"
if [[ ${EXECUTABLE} == "docker" && $(uname -s || true) == *_NT* ]]; then
    : "${LOCAL_DIR="$(cmd.exe /c CD || true)"}"
    : "${WORK_DIR:="c:\\mnt\\${PROJECT:-"workdir"}"}"
else
    : "${LOCAL_DIR="$(pwd || true)"}"
    : "${WORK_DIR:="//mnt/${PROJECT:-"workdir"}"}"
fi
if [[ ${EXECUTABLE} != "podman" ]]; then
    unset DOCKER_SECRETS
elif [[ -r ${HOME}/.netrc && -z ${DOCKER_SECRETS+x} ]]; then
    DOCKER_SECRETS=("--secret=id=.netrc,src=${HOME}/.netrc")
fi
# Replace `${COMMAND-}` with `${COMMAND-"bash"}` to make `bash` the default.
[[ $# -eq 0 ]] && eval set -- "${COMMAND-}"
unset CACHE
if [[ -n ${DOCKER_FILE} && ${IMAGE} == ${ARTIFACTORY_DOMAIN}/*-docker-dev-local/* && ${EXECUTABLE} == "podman" ]]; then
    : "${DEFAULT_BRANCH="main"}"
    : "${COMMIT_REF_NAME="$(git -C "$(dirname -- "${DOCKER_FILE}")" branch --show-current 2>/dev/null || true)"}"
    if [[ ${DEFAULT_BRANCH} != "${COMMIT_REF_NAME}" ]]; then
        CACHE+=("--cache-from=${IMAGE_PATH}/cache/${DEFAULT_BRANCH}")
    fi
    if [[ -n ${COMMIT_REF_NAME} ]]; then
        CACHE+=("--cache-from=${IMAGE_PATH}/cache/${COMMIT_REF_NAME}")
        if [[ -z ${NO_WRITE_CACHE+x} ]]; then
            CACHE+=("--cache-to=${IMAGE_PATH}/cache/${COMMIT_REF_NAME}")
        fi
    fi
fi
[[ ${EXECUTABLE} == "podman" ]] && : "${USERNS="keep-id"}"
# Feedback
[[ -n ${SHOW_HELP-} ]] && echo "${USAGE}"
echo "${PROJECT+"${PROJECT}: "}${DOCKER_DIR:+"${DOCKER_DIR} "}${DOCKER_FILE-}"
echo "Image: ${IMAGE-}"
[[ -n ${LOCAL_DIR-} ]] && echo "mount ${WORK_DIR} -> ${LOCAL_DIR}"
[[ -n ${ENTRYPOINT-}${*} ]] && echo "Command:${ENTRYPOINT+" \'--entrypoint=${ENTRYPOINT}\'"} ${*@Q}"
[[ -n ${CACHE-} ]] && echo -e "Caching: ${CACHE[*]/#/\\n\\t}"

: "${IMAGE:?"An image is required."}"

# Build
if [[ -n ${DOCKER_FILE-} ]]; then
    (
        set -x
        ${SHOW_HELP+: }"${EXECUTABLE}" build \
            "${DOCKER_BUILD_ARGS[@]}" \
            "${CACHE[@]}" \
            "--file=${DOCKER_FILE}" \
            "${DOCKER_SECRETS[@]}" \
            "--tag=${IMAGE}" \
            ${DOCKER_DIR:+"${DOCKER_DIR}"}
    )
fi

# Run
(
    set -x
    ${SHOW_HELP+: }"${EXECUTABLE}" run \
        ${ENTRYPOINT+"--entrypoint=${ENTRYPOINT}"} \
        "--interactive" \
        ${LOCAL_DIR+"--mount=type=bind,source=${LOCAL_DIR},target=${WORK_DIR}"} \
        "--name=${PROJECT}" \
        "--rm" \
        "--tty" \
        ${USER_IN_CONTAINER+"--user=${USER_IN_CONTAINER}"} \
        ${USERNS+"--userns=${USERNS}"} \
        ${LOCAL_DIR+"--workdir=${WORK_DIR}"} \
        "${IMAGE}" \
        "$@"
)
