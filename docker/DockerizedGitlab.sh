#!/usr/bin/env bash
set -Eeuo pipefail

# Parse parameters
# See `man getopt'. https://gist.github.com/bobpaul/ecd74cdf7681516703f20726431eaceb
SHORT="p::n::h::l::i::r"
LONG="password::,root-password::,name::,home::,hostname::,image::,remove,no-remove,https,no-https"

! (getopt --test >/dev/null || [[ $? -ne 4 ]]) || ! echo getopt is outdated
PARSED=$(getopt --options "${SHORT}" --longoptions "${LONG}" --name "$0" -- "$@")
eval set -- "${PARSED}"
while [[ $# -gt 0 ]]; do
    case "$1" in
    -p | --password | --root-password)
        shift
        GITLAB_ROOT_PASSWORD="$1"
        ;;
    -i | --image)
        shift
        GITLAB_IMAGE="$1"
        ;;
    -r | --remove)
        GITLAB_REMOVE="true"
        ;;
    --no-remove)
        GITLAB_REMOVE="false"
        ;;
    --https)
        GITLAB_HTTPS="true"
        ;;
    --no-https)
        GITLAB_HTTPS="false"
        ;;
    --)
        shift
        break
        ;;
    *)
        echo "Error:  Cannot parse '$*'"
        exit 1
        ;;
    esac
    shift
done

# Forcefully clean up old instances

if [[ -n ${GITLAB_NAME-} ]]; then
    podman container rm --force --ignore --volumes "${GITLAB_NAME}"
    if [[ -e ${GITLAB_HOME-} ]]; then
        rm -rf -- "${GITLAB_HOME}" 2>/dev/null || true
        if [[ -e ${GITLAB_HOME} ]]; then
            sudo -n -- rm -rf -- "${GITLAB_HOME}"
        fi
    fi
fi

GITLAB_NAME="gitlab"
GITLAB_HOME="${HOME}/.gitlab/${GITLAB_NAME}"

podman container rm --force --ignore --volumes "${GITLAB_NAME}"
if [[ -e ${GITLAB_HOME} ]]; then
    rm -rf -- "${GITLAB_HOME}" 2>/dev/null || true
    if [[ -e ${GITLAB_HOME} ]]; then
        sudo -n -- rm -rf -- "${GITLAB_HOME}"
    fi
fi

[[ ${GITLAB_REMOVE-} == true ]] && exit

# Defaults
: "${GITLAB_HTTPS:="false"}"
: "${GITLAB_IMAGE:="docker.io/gitlab/gitlab-ce:14.10.5-ce.0"}"
: "${GITLAB_ROOT_PASSWORD:="${CI_JOB_TOKEN:-"Gitlab"}"}"
DUMMY_PASS="00000000" # Guarantee 8 characters
GITLAB_ROOT_PASSWORD="${GITLAB_ROOT_PASSWORD}${DUMMY_PASS:${#GITLAB_ROOT_PASSWORD}:${#DUMMY_PASS}}"
GITLAB_HOSTNAME="localhost"
HTTPS_PORT="8443"
HTTP_PORT="8080"
REGISTRY_PORT="5050"
SSH_PORT="8022"

# Generated configuration

if [[ ${GITLAB_HTTPS} == true ]]; then
    PROTOCOL="https"
    GITLAB_PORT="${HTTPS_PORT}"
else
    PROTOCOL="http"
    GITLAB_PORT="${HTTP_PORT}"
fi
GITLAB_HOST="${PROTOCOL}://${GITLAB_HOSTNAME}"
REGISTRY_HOST="${PROTOCOL}://${GITLAB_HOSTNAME}:${REGISTRY_PORT}"
GITLAB_OMNIBUS_CONFIG="\
external_url '${GITLAB_HOST}'; \
registry_external_url '${REGISTRY_HOST}'\
"

# Start fresh instance

mkdir -p "${GITLAB_HOME}"/{config,data,logs}

(
    set -x
    podman run \
        "--detach" \
        "--env=GITLAB_HOST=${GITLAB_HOST}" \
        "--env=GITLAB_OMNIBUS_CONFIG=${GITLAB_OMNIBUS_CONFIG}" \
        "--env=GITLAB_ROOT_PASSWORD=${GITLAB_ROOT_PASSWORD}" \
        "--hostname=${GITLAB_HOSTNAME}" \
        "--mount=type=bind,src=${GITLAB_HOME}/config,target=/etc/gitlab" \
        "--mount=type=bind,src=${GITLAB_HOME}/data,target=/var/opt/gitlab" \
        "--mount=type=bind,src=${GITLAB_HOME}/logs,target=/var/log/gitlab" \
        "--name=${GITLAB_NAME}" \
        "--no-healthcheck" \
        "--publish=${HTTPS_PORT}:443" \
        "--publish=${HTTP_PORT}:80" \
        "--publish=${REGISTRY_PORT}:${REGISTRY_PORT}" \
        "--publish=${SSH_PORT}:22" \
        "--rm" \
        "--shm-size=256m" \
        "--user=root" \
        "--userns=keep-id" \
        "${GITLAB_IMAGE}" \
        "$@"
)

# Save parameters used

cat >|"${GITLAB_HOME}/env" <<EOF
export GITLAB_NAME="${GITLAB_NAME}"
export GITLAB_HOME="${GITLAB_HOME}"
export GITLAB_HOST="${GITLAB_HOST}:${GITLAB_PORT}"
export GITLAB_ROOT_PASSWORD="${GITLAB_ROOT_PASSWORD}"
until curl --insecure --silent --show-error --fail "\${GITLAB_HOST}"; do sleep 2; done; echo
GITLAB_MOVE_ITEM_PRIVATE_TOKEN="\$(etc/gitlab_get_api_token.py)"
export GITLAB_MOVE_ITEM_PRIVATE_TOKEN
export GITLAB_MOVE_ITEM_URL="${GITLAB_HOST}"
echo "\${GITLAB_MOVE_ITEM_URL}"
unset GITLAB_HOST
unset GITLAB_ROOT_PASSWORD
EOF

echo "sudo socat tcp-listen:80,reuseaddr,fork tcp:${GITLAB_HOSTNAME}:${GITLAB_PORT} &"
echo . "${GITLAB_HOME}/env"
