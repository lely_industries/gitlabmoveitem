#!/usr/bin/env python3
"""."""
import os
from urllib.parse import urljoin

import requests
from bs4 import BeautifulSoup


class GitlabSession(requests.Session):
    """."""

    SIGN_IN = "/users/sign_in"
    PERSONAL_ACCESS_TOKENS = "/-/profile/personal_access_tokens"

    def __init__(self, url=None):
        """."""
        super().__init__()
        self.verify = False
        self.url = url
        self.csrf = {}

    @property
    def url(self):
        """."""
        return self._url

    @url.setter
    def url(self, value):
        self._url = value.lstrip("/")

    @property
    def csrf(self):
        """."""
        return self._csrf

    @csrf.setter
    def csrf(self, value):
        self._csrf = value

    def urljoin(self, path):
        """."""
        return urljoin(self.url, path)

    def update_csrf_token(self, response):
        """."""
        soup = BeautifulSoup(response.text, "html.parser")
        token = soup.find(attrs={"name": "csrf-token"})
        param = soup.find(attrs={"name": "csrf-param"})
        data = {param.get("content"): token.get("content")}
        self.csrf.update(data)

    @staticmethod
    def get_authenticity_token(response):
        """."""
        soup = BeautifulSoup(response.text, "html.parser")
        token = soup.find("input", attrs={"name": "authenticity_token", "type": "hidden"}).get(
            "value"
        )
        return {"authenticity_token": token}

    def log_in(self, username, password):
        """."""
        url = self.urljoin(self.SIGN_IN)
        response = self.get(url)
        response.raise_for_status()
        self.update_csrf_token(response)

        data = {
            "user[login]": username,
            "user[password]": password,
            "user[remember_me]": 0,
            "utf8": "✓",
        }
        data.update(self.csrf)
        response = self.post(url, data=data)
        response.raise_for_status()

    def get_personal_access_token(self):
        """."""
        url = self.urljoin(self.PERSONAL_ACCESS_TOKENS)
        response = self.get(url)
        response.raise_for_status()
        self.update_csrf_token(response)
        authenticity_token = self.get_authenticity_token(response)

        data = {
            "personal_access_token[expires_at]": "",
            "personal_access_token[name]": "python-gitlab",
            "personal_access_token[scopes][]": [
                "api",
                "write_repository",
                "sudo",
            ],
        }
        data.update(self.csrf)
        data.update(authenticity_token)
        response = self.post(url, data=data)
        response.raise_for_status()
        self.update_csrf_token(response)
        soup = BeautifulSoup(response.text, "html.parser")
        token = soup.find("input", id="created-personal-access-token").get("value")
        return token


gitlab_url = os.environ.get("GITLAB_HOST", "http://localhost/")
gitlab_root_password = os.environ.get("GITLAB_ROOT_PASSWORD")
with GitlabSession(gitlab_url) as session:
    session.log_in("root", gitlab_root_password)
    access_token = session.get_personal_access_token()
    print(access_token)
